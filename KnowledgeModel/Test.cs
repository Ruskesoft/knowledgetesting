﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeModel
{
    /// <summary>
    /// Состояние сеанса тестирования
    /// </summary>
    public enum TestState
    {
        New,
        Started,
        Finished
    }
    
    /// <summary>
    /// Сеанс тестирования
    /// </summary>
    public class Test
    {
        #region Поля

        internal IDataProvider DataProvider;

        int testID;
        User user;
        DateTime startDate;
        DateTime finishDate;
        int currentQuestionID;
        TestState state;
        int[] testQuestionsIDs;
        int[] testQuestionsIsCorrectIDs;
        int[] testQuestionsNotAnswerIDs;

        #endregion


        #region Конструктор

        /// <summary>
        /// Подготовка сеанса тестирования
        /// </summary>
        public Test(IDataProvider DataProvider, User user)
        {
            if (user != null && user.Exists)
            {
                this.DataProvider = DataProvider;
                this.user = user;

                testID = DataProvider.GetUserTestID(user.UserID);

                Renew();

                if (testID == 0)
                {
                    // Новый сеанс тестирования
                    testID = DataProvider.CreateTest(user.UserID);

                    if (testID == 0)
                    {
                        throw new Exception("Не удалось подготовить сеанс тестирования!");
                    }
                }
            }
            else
            {
                throw new Exception("Пользователь не существует!");
            }
        }


        #endregion


        #region События


        #endregion

        #region Свойства

        /// <summary>
        /// Идентификатор теста
        /// </summary>
        public int TestID
        {
            get
            {
                return testID;
            }
        }


        /// <summary>
        /// Пользователь, проходящий тест
        /// </summary>
        public User User
        {
            get
            {
                return user;
            }
        }


        /// <summary>
        /// Время начала теста
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
        }

        /// <summary>
        /// Время окончания теста
        /// </summary>
        public DateTime FinishDate
        {
            get { return finishDate; }
        }

        /// <summary>
        /// Текущее состояние сеанса тестирования
        /// </summary>
        public TestState State
        {
            get { return state; }
        }

        /// <summary>
        /// Новый тест?
        /// </summary>
        public bool IsNew
        {
            get { return state == TestState.New; }
        }

        /// <summary>
        /// Тест начат?
        /// </summary>
        public bool IsStarted
        {
            get { return state == TestState.Started; }
        }

        /// <summary>
        /// Тест завершен?
        /// </summary>
        public bool IsFinished
        {
            get { return state == TestState.Finished; }
        }



        /// <summary>
        /// Номер текущего вопроса
        /// </summary>
        public int CurrentQuestionNumber
        {
            get
            {
                if (testQuestionsIDs != null)
                {
                    int index = Array.IndexOf(testQuestionsIDs, currentQuestionID);

                    return index + 1;
                }
                return 0;
            }
        }


        /// <summary>
        /// Текущий вопрос
        /// </summary>
        public Question CurrentQuestion
        {
            get
            {
                if (currentQuestionID > 0)
                {
                    return new Question(DataProvider, currentQuestionID);
                }
                return null;
            }
        }



        /// <summary>
        /// Количество вопросов в тестировнии
        /// </summary>
        public int QuestionMax
        {
            get { return DataProvider.MaxQuestions; }
        }

        /// <summary>
        /// Минимальное количество правильных ответов для зачета
        /// </summary>
        public int QuestionMinCorrectAnswer
        {
            get { return DataProvider.MinCorrectAnswer; }
        }

        /// <summary>
        /// Количество заданых вопросов
        /// </summary>
        public int QuestionCount
        {
            get
            {
                return testQuestionsIDs.Length;
            }
        }

        /// <summary>
        /// Количество вопросов на которые получен ответ
        /// </summary>
        public int QuestionAnsweredCount
        {
            get
            {
                return (testQuestionsIDs.Length - testQuestionsNotAnswerIDs.Length);
            }
        }

        /// <summary>
        /// Количество вопросов на которые получен правильный ответ
        /// </summary>
        public int QuestionAnsweredIsCorrectCount
        {
            get
            {
                return testQuestionsIsCorrectIDs.Length;
            }
        }

        /// <summary>
        /// Количество пропущенных вопросов
        /// </summary>
        public int QuestionNotAnsweredCount
        {
            get
            {
                return testQuestionsNotAnswerIDs.Length;
            }
        }


        /// <summary>
        /// Общий процент выполнения
        /// </summary>
        public int CompletionPercent
        {
            get
            {
                if (QuestionMax == 0) return 0;

                return QuestionAnsweredCount * 100 / QuestionMax;
            }
        }

        /// <summary>
        /// Процент правильных ответов
        /// </summary>
        public int CorrectAnswerPercent
        {
            get
            {
                if (QuestionAnsweredCount == 0) return 0;

                return QuestionAnsweredIsCorrectCount * 100 / QuestionAnsweredCount;
            }
        }



        /// <summary>
        /// Стадия тестирования
        /// </summary>
        public string StateTitle
        {
            get
            {
                switch (state)
                {
                    case TestState.New:
                        return "Тестирование не начато.";

                    case TestState.Started:
                        return "Тестирование идет...";

                    case TestState.Finished:
                        return "Тестирование завершено!";
                }

                return "Не известно!";
            }
        }

        /// <summary>
        /// Результат теста зачтен?
        /// </summary>
        public bool ResultPassed
        {
            get { return QuestionAnsweredIsCorrectCount >= QuestionMinCorrectAnswer; }
        }

        /// <summary>
        /// Оценка за тестирование
        /// </summary>
        public int ResultMark
        {
            get
            {
                return CorrectAnswerPercent / 20;
            }
        }

        /// <summary>
        /// Результат тестирования
        /// </summary>
        public string ResultTitle
        {
            get
            {
                if (IsFinished)
                {
                    if (CompletionPercent == 100)
                    {
                        if (CorrectAnswerPercent == 100)
                        {
                            return "Тест зачтён! Вы показали БЛЕСТЯЩИЕ знания, ответив правильно на все вопросы!";
                        }
                        else if (CorrectAnswerPercent > 80)
                        {
                            return "Тест зачтён! Вы показали отличные знания, ответив правильно почти на все вопросы!";
                        }
                        else if (CorrectAnswerPercent > 60)
                        {
                            return "Тест зачтён! Вы показали хорошие знания, ответив на большую часть вопросов!";
                        }
                        else if (CorrectAnswerPercent > 40)
                        {
                            return "Тест зачтён. Вы показали удовлетворительные знания, допустив много ошибок!";
                        }
                        else if (CorrectAnswerPercent > 20)
                        {
                            return "Тест НЕ зачтён. Вы показали не удовлетворительные знания, необходимо еще подготовиться!";
                        }
                        else if (CorrectAnswerPercent > 10)
                        {
                            return "Тест НЕ зачтён. Вы показали не удовлетворительные знания, необходимо готовиться!";
                        }
                        else
                        {
                            return "Тест НЕ зачтён. Вы показали полное отсутствие знаний, необходимо готовиться!";
                        }
                    }
                }

                return "";
            }
        }

        /// <summary>
        /// Описание результата тестирования тестирования
        /// </summary>
        public string ResultDescription
        {
            get
            {
                if (IsFinished)
                {
                    if (CompletionPercent == 100)
                    {
                        return string.Format("На {1} из {2} вопросов получен правильный ответ. Время затраченное на тестирование: {3} мин. {4} сек.",
                            CompletionPercent, QuestionAnsweredIsCorrectCount, QuestionMax, (finishDate - startDate).Minutes, (finishDate - startDate).Seconds);
                    }
                }

                return string.Format("{0}% выполено. На {1} из {2} вопросов получен ответ.",
                    CompletionPercent, QuestionAnsweredCount, QuestionMax);
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Обновить свойства объекта тест
        /// </summary>
        public void Renew()
        {
            startDate = DateTime.MinValue;
            finishDate = DateTime.MinValue;

            testQuestionsIDs = null;
            testQuestionsNotAnswerIDs = null;

            state = TestState.New;

            if (testID > 0)
            {
                DataProvider.GetTest(testID, out startDate, out finishDate, out currentQuestionID);

                if (startDate > DateTime.MinValue)
                {
                    if (finishDate == DateTime.MinValue)
                    {
                        state = TestState.Started;
                    }
                    else
                    {
                        state = TestState.Finished;
                    }
                }

                testQuestionsIDs = DataProvider.GetTestQuestionsIDs(testID);
                testQuestionsIsCorrectIDs = DataProvider.GetTestQuestionsIsCorrectIDs(testID);
                testQuestionsNotAnswerIDs = DataProvider.GetTestQuestionsNotAnswerIDs(testID);
            }
        }

        /// <summary>
        /// Начать тест
        /// </summary>
        public void Start()
        {
            if (!IsStarted)
            {
                if (!IsFinished)
                {
                    if (DataProvider.StartTest(testID))
                    {
                        // Первый вопрос
                        NextQuestion();
                    }
                    else
                    {
                        throw new Exception("Не удалось завершить тест!");
                    }
                }
                else
                {
                    throw new Exception("Тестирование было завершено!");
                }
            }
            else
            {
                throw new Exception("Тестирование уже начато!");
            }
        }


        /// <summary>
        /// Сохранить вариант ответа
        /// </summary>
        public void SetAnswer(Answer answer)
        {
            if (DataProvider.SetQuestionAnswer(testID, currentQuestionID, answer.AnswerID))
            {
                Renew();
            }
            else
            {
                throw new Exception("Не сохранить вариант ответа!");
            }
        }


        /// <summary>
        /// Следующий вопрос
        /// </summary>
        public void NextQuestion()
        {
            bool next = false;

            if (QuestionCount < QuestionMax)
            {
                // Следующий новый вопрос

                int[] allQuestionIDs = DataProvider.GetQuestionsIDs();
                int[] freeQuestionIDs = allQuestionIDs.Where(q => !testQuestionsIDs.Contains(q)).ToArray();

                if (freeQuestionIDs.Length > 0)
                {
                    Random rnd = new Random();

                    currentQuestionID = freeQuestionIDs[rnd.Next(0, freeQuestionIDs.Length - 1)];

                    next = DataProvider.AddTestQuestion(testID, currentQuestionID);
                }
            }
            else
            {
                if (testQuestionsNotAnswerIDs.Length > 0)
                {
                    // Следующий неотвеченный вопрос

                    int index = Array.IndexOf(testQuestionsNotAnswerIDs, currentQuestionID);

                    if (index < 0 || index >= QuestionNotAnsweredCount - 1)
                    {
                        currentQuestionID = testQuestionsNotAnswerIDs[0];
                    }
                    else
                    {
                        currentQuestionID = testQuestionsNotAnswerIDs[index + 1];
                    }

                    next = true;
                }
                else
                {
                    // Достигрут конец списка вопросов
                    Finish();
                }
            }

            if (next)
            {
                if (DataProvider.SetCurrentQuestion(testID, currentQuestionID))
                {
                    Renew();
                }
                else
                {
                    throw new Exception("Не удалось перейти к следующему вопросу!");
                }
            }
        }

        /// <summary>
        /// Завершить тест
        /// </summary>
        public void Finish()
        {
            if (IsStarted)
            {
                if (!IsFinished)
                {
                    if (DataProvider.FinishTest(testID))
                    {
                        Renew();
                    }
                    else
                    {
                        throw new Exception("Не удалось завершить тест!");
                    }
                }
                else
                {
                    throw new Exception("Тестирование уже завершено!");
                }
            }
            else
            {
                throw new Exception("Тестирование еще не начато!");
            }
        }


        #endregion

    }
}