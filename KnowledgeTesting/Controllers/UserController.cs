﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model = KnowledgeModel;

namespace KnowledgeTesting.Controllers
{
    public class UserController : Controller
    {
        Model.Manager manager = new Model.Manager(MvcApplication.DataProvider);

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration(string UserName)
        {
            if (!string.IsNullOrEmpty(UserName))
            {
                var user = manager.CreateUser(UserName);

                if (user.Exists)
                {
                    return View(user);
                }
            }

            return RedirectToAction("Index", "Home");
        }
    }
}