﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Data;

namespace KnowledgeModel
{
    public class PostgreeDataProvider : IDataProvider
    {
        public PostgreeDataProvider()
        {
        }

        public PostgreeDataProvider(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }

        public string ConnectionString { get; set; }

        public int MaxQuestions { get { return 5; } }

        public int MinCorrectAnswer { get { return 3; } }


        public DataTable GetUsers()
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_users", cn);
            cm.CommandType = CommandType.StoredProcedure;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "users");
            cn.Close();
            return ds.Tables["users"];
        }

        public int GetUserID(string userName)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_user_id", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@user_name", NpgsqlTypes.NpgsqlDbType.Text).Value = userName;
            cn.Open();
            var result = cm.ExecuteScalar();
            cn.Close();
            return result != DBNull.Value ? (int)result : 0;
        }

        public int CreateUser(string userName)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("create_user", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@user_name", NpgsqlTypes.NpgsqlDbType.Text).Value = userName;
            cn.Open();
            int userID = (int)cm.ExecuteScalar();
            cn.Close();
            return userID;
        }


        public int CreateTest(int userID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("create_test", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@user_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = userID;
            cn.Open();
            var result = cm.ExecuteScalar();
            cn.Close();
            return result != DBNull.Value ? (int)result : 0;
        }


        public int GetUserTestID(int userID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_user_test_id", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@user_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = userID;
            cn.Open();
            var result = cm.ExecuteScalar();
            cn.Close();
            return result != DBNull.Value ? (int)result : 0;
        }


        public void GetTest(int testID, out DateTime startDate, out DateTime finishDate, out int currentQuestionID)
        {
            startDate = DateTime.MinValue;
            finishDate = DateTime.MinValue;
            currentQuestionID = 0;
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_test", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "test");
            cn.Close();
            DataTable resultTable = ds.Tables["test"];
            if (resultTable != null && resultTable.Rows.Count > 0)
            {
                var row = resultTable.Rows[0];
                if(row["start_date"] != DBNull.Value)
                    startDate = (DateTime)row["start_date"];
                if (row["finish_date"] != DBNull.Value)
                    finishDate = (DateTime)row["finish_date"];
                if (row["current_question_id"] != DBNull.Value)
                    currentQuestionID = (int)row["current_question_id"];
            }
        }


        public int[] GetQuestionsIDs()
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_questions_ids", cn);
            cm.CommandType = CommandType.StoredProcedure;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "questions");
            cn.Close();
            DataTable resultTable = ds.Tables["questions"];
            if (resultTable != null)
            {
                return (from DataRow row in resultTable.Rows
                        select (int)row["question_id"]).ToArray();
            }
            return null;
        }

        public int[] GetTestQuestionsIDs(int testID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_test_questions_ids", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "questions");
            cn.Close();
            DataTable resultTable = ds.Tables["questions"];
            if (resultTable != null)
            {
                return (from DataRow row in resultTable.Rows
                        select (int)row["question_id"]).ToArray();
            }
            return null;
        }

        public int[] GetTestQuestionsIsCorrectIDs(int testID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_test_questions_iscorrect_ids", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "questions");
            cn.Close();
            DataTable resultTable = ds.Tables["questions"];
            if (resultTable != null)
            {
                return (from DataRow row in resultTable.Rows
                        select (int)row["question_id"]).ToArray();
            }
            return null;
        }

        public int[] GetTestQuestionsNotAnswerIDs(int testID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_test_questions_notanswer_ids", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "questions");
            cn.Close();
            DataTable resultTable = ds.Tables["questions"];
            if (resultTable != null)
            {
                return (from DataRow row in resultTable.Rows
                        select (int)row["question_id"]).ToArray();
            }
            return null;
        }


        public string GetQuestionText(int questionID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_question_text", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@question_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = questionID;
            cn.Open();
            string questionText = cm.ExecuteScalar().ToString();
            cn.Close();
            return questionText;
        }


        public DataTable GetQuestionAnswers(int questionID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("get_question_answers", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@question_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = questionID;
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataSet ds = new DataSet();
            cn.Open();
            da.SelectCommand = cm;
            da.Fill(ds, "answers");
            cn.Close();
            return ds.Tables["answers"];
        }


        public bool StartTest(int testID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("start_test", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            cn.Open();
            bool result = (bool)cm.ExecuteScalar();
            cn.Close();
            return result;
        }

        public bool SetCurrentQuestion(int testID, int questionID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("set_current_question", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            cm.Parameters.Add("@question_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = questionID;
            cn.Open();
            bool result = (bool)cm.ExecuteScalar();
            cn.Close();
            return result;
        }

        public bool AddTestQuestion(int testID, int questionID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("add_test_question", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            cm.Parameters.Add("@question_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = questionID;
            cn.Open();
            bool result = (bool)cm.ExecuteScalar();
            cn.Close();
            return result;
        }

        public bool SetQuestionAnswer(int testID, int questionID, int answerID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("set_question_answer", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            cm.Parameters.Add("@question_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = questionID;
            cm.Parameters.Add("@answer_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = answerID;
            cn.Open();
            bool result = (bool)cm.ExecuteScalar();
            cn.Close();
            return result;
        }

        public bool FinishTest(int testID)
        {
            NpgsqlConnection cn = new NpgsqlConnection(ConnectionString);
            NpgsqlCommand cm = new NpgsqlCommand("finish_test", cn);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.Add("@test_id", NpgsqlTypes.NpgsqlDbType.Integer).Value = testID;
            cn.Open();
            bool result = (bool)cm.ExecuteScalar();
            cn.Close();
            return result;
        }

    }
}