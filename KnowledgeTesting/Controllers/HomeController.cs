﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model = KnowledgeModel;


namespace KnowledgeTesting.Controllers
{
    public class HomeController : Controller
    {
        Model.Manager manager = new Model.Manager(MvcApplication.DataProvider);

        // GET: Home
        public ActionResult Index()
        {
            Session["UserName"] = null;

            ViewBag.UserNames = manager.GetUsers().Select(u => u.UserName).ToArray();

            return View();
        }

        [HttpPost]
        public ActionResult Index(string userName)
        {
            return View("Test", manager.GetUser(userName));
        }
    }
}