﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KnowledgeModel
{
    /// <summary>
    /// Вопрос тестирования
    /// </summary>
    public class Question
    {
        #region Поля

        internal IDataProvider DataProvider;

        int questionID;
        string questionText;

        #endregion


        #region Конструктор

        internal Question(IDataProvider DataProvider, int questionID)
        {
            this.DataProvider = DataProvider;
            this.questionID = questionID;
        }

        #endregion


        #region Свойства

        /// <summary>
        /// Идентификатор вопроса
        /// </summary>
        public int QuestionID
        {
            get { return questionID; }
        }

        /// <summary>
        /// Текст вопроса
        /// </summary>
        public string QuestionText
        {
            get
            {
                if (string.IsNullOrEmpty(questionText))
                {
                    questionText = DataProvider.GetQuestionText(questionID);
                }
                return questionText;
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Вернуть список вариантов ответов на вопрос
        /// </summary>
        public List<Answer> GetAnswers()
        {
            var answers = DataProvider.GetQuestionAnswers(questionID);

            if (answers != null)
            {
                return (from DataRow r in answers.Rows
                        select new Answer(questionID, (int)r["answer_id"], (string)r["answer_text"])).ToList();
            }

            return null;
        }

        #endregion
    }
}