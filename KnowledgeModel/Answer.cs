﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeModel
{
    /// <summary>
    /// Вариант ответа на вопрос
    /// </summary>
    public class Answer
    {
        #region Поля

        int questionID;
        int answerID;
        string answerText;

        #endregion


        #region Конструктор

        internal Answer(int questionID, int answerID, string answerText)
        {
            this.questionID = questionID;
            this.answerID = answerID;
            this.answerText = answerText;
        }

        #endregion


        #region Свойства

        /// <summary>
        /// Идентификатор ответа
        /// </summary>
        public int AnswerID
        {
            get { return answerID; }
        }

        /// <summary>
        /// Текст ответа
        /// </summary>
        public string AnswerText
        {
            get
            {
                return answerText;
            }
        }

        #endregion
    }
}