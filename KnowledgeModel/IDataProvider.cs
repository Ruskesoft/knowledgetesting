﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeModel
{
    /// <summary>
    /// Провайдер данных
    /// </summary>
    public interface IDataProvider
    {
        int MaxQuestions { get; }
        int MinCorrectAnswer { get; }

        /// <summary>
        /// Вернуть таблицу зарегистрированных пользователей
        /// </summary>
        /// <returns>Таблица пользователей</returns>
        DataTable GetUsers();

        /// <summary>
        /// Создать нового пользователя и вернуть его идентификатор
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Идентификатор пользователя</returns>
        int CreateUser(string userName);

        /// <summary>
        /// Вернуть идентификатор существующего пользователя по имени
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Идентификатор пользователя</returns>
        int GetUserID(string userName);


        /// <summary>
        /// Создать новый сеанс тестирования и вернуть его идентификатор
        /// </summary>
        /// <param name="userID">Идентификатор пользователя</param>
        /// <returns>Идентификатор сеанса тестирования</returns>
        int CreateTest(int userID);

        /// <summary>
        /// Вернуть идентификатор существующего сеанса тестирования пользователя
        /// </summary>
        /// <param name="userID">Идентификатор пользователя</param>
        /// <returns>Идентификатор сеанса тестирования</returns>
        int GetUserTestID(int userID);


        /// <summary>
        /// Старт тестирования
        /// </summary>
        /// <param name="testID">Идентификатор сеанса тестирования</param>
        /// <returns>Время начала тестирования</returns>
        bool StartTest(int testID);

        /// <summary>
        /// Добавить новый вопрос тестирования
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <param name="questionID">Идентификатор вопроса</param>
        bool AddTestQuestion(int testID, int questionID);

        /// <summary>
        /// Установить текущий вопрос тестирования
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <param name="questionID">Идентификатор вопроса</param>
        bool SetCurrentQuestion(int testID, int questionID);

        /// <summary>
        /// Сохранить ответ на вопрос
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <param name="questionID">Иденитификатор вопроса</param>
        /// <param name="answerID">Идентификатор выбранного ответа</param>
        bool SetQuestionAnswer(int testID, int questionID, int answerID);

        /// <summary>
        /// Завершение тестирования
        /// </summary>
        /// <param name="testID">Идентификатор сеанса тестирования</param>
        /// <returns>Время окончания тестирования</returns>
        bool FinishTest(int testID);


        /// <summary>
        /// Свойства сеанса тестирования
        /// </summary>
        void GetTest(int testID, out DateTime startDate, out DateTime finishDate, out int currentQuestionID);

        /// <summary>
        /// Вернуть список всех вопросов
        /// </summary>
        /// <returns>Список идентификаторов вопросов</returns>
        int[] GetQuestionsIDs();

        /// <summary>
        /// Вернуть список заданных вопросов тестирования
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <returns>Список идентификаторов вопросов</returns>
        int[] GetTestQuestionsIDs(int testID);

        /// <summary>
        /// Вернуть список вопросов тестирования, на которые получен правильный ответ
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <returns>Список идентификаторов вопросов</returns>
        int[] GetTestQuestionsIsCorrectIDs(int testID);

        /// <summary>
        /// Вернуть список не отвеченных вопросов тестирования
        /// </summary>
        /// <param name="testID">Идентификатор тестирования</param>
        /// <returns>Список идентификаторов вопросов</returns>
        int[] GetTestQuestionsNotAnswerIDs(int testID);


        /// <summary>
        /// Вернуть текст вопроса
        /// </summary>
        /// <param name="questionID">Идентификатор вопроса</param>
        string GetQuestionText(int questionID);

        /// <summary>
        /// Вернуть таблицу вариантов ответов на вопрос
        /// </summary>
        /// <param name="questionID">Идентификатор вопроса</param>
        /// <returns>Таблица вариантов ответов</returns>
        DataTable GetQuestionAnswers(int questionID);

    }
}
