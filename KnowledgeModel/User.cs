﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnowledgeModel
{
    /// <summary>
    /// Тестируемый пользователь
    /// </summary>
    public class User
    {
        #region Поля

        int userID;
        string userName;

        #endregion


        #region Конструктор


        internal User(int userID, string userName)
        {
            this.userID = userID;
            this.userName = userName;
        }


        #endregion


        #region Свойства

        public int UserID
        {
            get { return userID; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public bool Exists
        {
            get { return userID > 0; }
        }

        #endregion

    }
}