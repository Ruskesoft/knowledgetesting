﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model = KnowledgeModel;

namespace KnowledgeTesting.Controllers
{
    public class TestController : Controller
    {
        Model.Manager manager = new Model.Manager(MvcApplication.DataProvider);

        string UserName
        {
            get
            {
                if (Session["UserName"] != null)
                {
                    return Session["UserName"].ToString();
                }

                return "";
            }
            set
            {
                Session["UserName"] = value;
            }
        }



        public ActionResult Index(string UserName)
        {
            if (!string.IsNullOrEmpty( UserName))
            {
                this.UserName = UserName;
            
                Model.Test test = manager.CreateTest(UserName);

                return View(test);
            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult Started(string AnswerText)
        {
            string userName = this.UserName;

            if (userName != "")
            {
                Model.Test test = manager.GetTest(userName);

                if (!test.IsFinished)
                {
                    if (test.IsNew)
                    {
                        test.Start();
                    }
                    else if (test.IsStarted)
                    {
                        if (!string.IsNullOrEmpty(AnswerText))
                        {
                            if (AnswerText != "Next")
                            {
                                Model.Answer answer = test.CurrentQuestion.GetAnswers().FirstOrDefault(a => a.AnswerText == AnswerText);

                                if (answer != null)
                                {
                                    test.SetAnswer(answer);
                                }
                            }

                            test.NextQuestion();
                        }
                    }

                    if (!test.IsFinished)
                    {
                        return View(test);
                    }
                    else
                    {
                        return RedirectToAction("Finished");
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Next()
        {
            return RedirectToAction("Started", new { AnswerText = "Next" });
        }

        public ActionResult Finished()
        {
            string userName = this.UserName;

            if (userName != "")
            {
                Model.Test test = manager.GetTest(userName);

                if (test.IsFinished)
                {
                    return View(test);
                }
            }

            return RedirectToAction("Index", "Home");
        }
    }
}