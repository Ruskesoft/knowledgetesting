﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnowledgeModel
{
    public class Manager
    {
        IDataProvider DataProvider;

        public Manager(IDataProvider DataProvider)
        {
            this.DataProvider = DataProvider;
        }



        public User[] GetUsers()
        {
            var usersTable = DataProvider.GetUsers();

            return (from DataRow row in usersTable.Rows
                    select new User((int)row["user_id"], (string)row["user_name"])).ToArray();
        }

        public User GetUser(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                int userID = DataProvider.GetUserID(userName);

                return new User(userID, userName);
            }
            return null;
        }

        public User CreateUser(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                int userID = DataProvider.CreateUser(userName);

                return new User(userID, userName);
            }
            return null;
        }

        public Test GetTest(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                User user = GetUser(userName);

                Test test = new Test(DataProvider, user);

                return test;
            }
            return null;
        }

        public Test CreateTest(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                User user = GetUser(userName);

                DataProvider.CreateTest(user.UserID);

                Test test = new Test(DataProvider, user);

                return test;
            }
            return null;
        }
    }
}
