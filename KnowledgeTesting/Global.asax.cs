﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Configuration;
using Model = KnowledgeModel;

namespace KnowledgeTesting
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static Model.IDataProvider DataProvider { get; private set; }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DataProvider = new Model.PostgreeDataProvider(ConfigurationManager.ConnectionStrings["Postgree"].ConnectionString);
        }
    }
}
